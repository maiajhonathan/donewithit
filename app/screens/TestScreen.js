import React, { useState } from 'react'
import { StyleSheet, View } from 'react-native'

import SafeAreaScreen from '../components/SafeAreaScreen';
import Card from '../components/Card';
import ListItem from '../components/ListItem';
import MessagesScreen from './MessagesScreen';
import colors from '../config/colors';
import ListingScreen from './ListingScreen';
import AccountScreen from './AccountScreen';
import AppTextInput from '../components/AppTextInput';
import AppPicker from '../components/AppPicker';
// import ListingDetails from './ListingDetails';
// import ViewImageScreen from './ViewImageScreen';

const categories = [
    { label: "Furniture", value: 1 },
    { label: "Clothing", value: 2 },
    { label: "Cameras", value: 3 },
]

const TestScreen = () => {

    const [category, setCategory] = useState();


    return (
        <SafeAreaScreen>
            <View style={ styles.wrapper }>
                {/* <Card
                    title="Red jacket for sale!"
                    subTitle="$100"
                    image={require("../assets/jacket.jpg")}
                /> */}

                {/* <ListingDetails
                    image={require("../assets/jacket.jpg")}
                    title="Red jacket for sale!"
                    price="$100"
                /> */}

                {/* <ViewImageScreen></ViewImageScreen> */}

                {/* <MessagesScreen /> */}
                {/* <ListingScreen /> */}
                {/* <AccountScreen /> */}
                {/* <AppTextInput icon="email" placeholder="Username"/> */}
                <AppPicker placeholder="Category" items={categories} selectedItem={category} onSelectItem={(item) => setCategory(item)}/>
            </View>
        </SafeAreaScreen>
    )
}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        // justifyContent: 'center',
        backgroundColor: '#e3e3e3',
    }
});

export default TestScreen
