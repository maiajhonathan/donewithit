export default {
    primary: "#fc5c65",
    secondary: "#4ECDC4",
    white: "#FFF",
    black: "#000",
    lightGrey: '#f8f4f4',
    midiumGrey: '#979797',
    darkGrey: "#6e6969",
    danger: '#ff5252',
    yellow: '#ffe66d'
};