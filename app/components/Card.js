import React from 'react'
import { View, Text, Image, StyleSheet, Platform } from 'react-native'
import colors from '../config/colors';

const Card = (props) => {
    const { title, subTitle, image } = props;

    // See goo way to use the Platform API.
    // Add box-shadow / Elevation to card

    return (
        <View style={styles.card}>
            <Image style={styles.image} source={image}/>
            <View style={styles.textWrapper}>
                <Text style={styles.title}>{ title }</Text>
                <Text style={styles.subTitle}>{ subTitle }</Text>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    card: {
        borderRadius: 20,
        backgroundColor: colors.white,
        overflow: 'hidden',
        marginVertical: 10
    },
    image: {
        width: "100%",
        height: 200
    },
    textWrapper: {  
        padding: 20,
    },
    title: {
        fontSize: 18
    },
    subTitle: {
        fontSize: 16,
        fontWeight: "700",
        marginTop: 10,
        color: colors.secondary
    }
})

export default Card
