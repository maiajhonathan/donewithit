import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

import {MaterialCommunityIcons} from '@expo/vector-icons'
import colors from '../config/colors'

const MenuItem = ({label,icon, iconSize, iconColor, iconBgColor}) => {
    return (
        <View style={styles.menuItem}>
            <View style={[styles.menuIconWrapper, { backgroundColor: iconBgColor }]}>
                <MaterialCommunityIcons name={icon} size={iconSize} color={iconColor}/>
            </View>
            <Text style={ styles.menuText }>{label}</Text>
        </View>
    )
}

const styles = StyleSheet.create({
    menuItem: {
        flexDirection: 'row',
        alignItems: 'center',

        paddingVertical: 10,
        paddingHorizontal: 15,

        marginBottom: 1,

        backgroundColor: colors.white,
    },
    menuIconWrapper: {
        padding: 8,
        borderRadius: 50,
    },
    menuText: {
        fontWeight: '700',
        fontSize: 16,
        marginLeft: 10,
    }
})

export default MenuItem