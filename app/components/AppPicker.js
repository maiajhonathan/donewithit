import React, { useState } from 'react'
import { Button, FlatList, Modal, StyleSheet, Text, View } from 'react-native'

import defaultStyles from "../config/styles"
import { MaterialCommunityIcons } from "@expo/vector-icons"
import { TouchableWithoutFeedback } from 'react-native'
import AppPickerItem from './AppPickerItem'

const AppPicker = ({ icon = "apps", items, onSelectItem, placeholder, selectedItem }) => {

    const [isVisible, setIsVisible] = useState(false);

    return (
        <>
        <TouchableWithoutFeedback onPress={() => setIsVisible(true)}>
            <View style={styles.container}>
                {icon && <MaterialCommunityIcons style={styles.icon} name={icon} color={defaultStyles.colors.midiumGrey} size={20}/>}
                <Text style={styles.text}>{selectedItem ? selectedItem.label : placeholder}</Text>
                {icon && <MaterialCommunityIcons name="chevron-down" color={defaultStyles.colors.midiumGrey} size={20}/>}
            </View>
        </TouchableWithoutFeedback>
        <Modal visible={isVisible} animationType="slide">
            <Button title="Close" onPress={() => setIsVisible(false)} />
            <FlatList 
            data={items}
            keyExtractor={(item) => item.value.toString()}
            renderItem={({ item }) => 
                <AppPickerItem 
                    label={item.label}
                    onPress={ () => {
                        setIsVisible(false);
                        onSelectItem(item);
                    }
                }
            />}
            />
        </Modal>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: defaultStyles.colors.lightGrey,
        borderRadius: 25,
        width: "100%",
        padding: 15,
        marginVertical: 10
    },
    text: {
        flex: 1
    },
    icon: {
        marginRight: 10
    },
})

export default AppPicker