import React from 'react'
import { StyleSheet, View } from 'react-native'

import defaultStyles from "../config/styles"
import { MaterialCommunityIcons } from "@expo/vector-icons"
import { TextInput } from 'react-native-gesture-handler'

const AppTextInput = ({icon, ...otherProps}) => {
    return (
        <View style={styles.container}>
            {icon && <MaterialCommunityIcons style={styles.icon} name={icon} color={defaultStyles.colors.midiumGrey} size={20}/>}
            <TextInput style={defaultStyles.text} {...otherProps}/>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: defaultStyles.colors.lightGrey,
        borderRadius: 25,
        width: "100%",
        padding: 15,
        marginVertical: 10
    },
    icon: {
        marginRight: 10
    },
})

export default AppTextInput