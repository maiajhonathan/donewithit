import React from 'react'
import { Image, StyleSheet, Text, TouchableHighlight, View } from 'react-native'
import { Swipeable } from 'react-native-gesture-handler'


import colors from '../config/colors'

const ListItem = ({title, description, image, onPress, renderRightActions}) => {    
    return (
        <Swipeable
            renderRightActions={renderRightActions}
        >
            <TouchableHighlight
                underlayColor={colors.lightGrey}
                onPress={onPress}
            >
                <View style={ styles.container }>
                    <Image style={ styles.avatar } source={ image }></Image>
                    <View style={ styles.textWrapper }>
                        <Text style={ styles.name }>{ title }</Text>
                        <Text style={ styles.description }>{ description }</Text>
                    </View>
                </View>
            </TouchableHighlight>
        </Swipeable>
    )
}

const styles = StyleSheet.create({
    container: {
        // position: "absolute",
        flexDirection: 'row',
        paddingVertical: 15
    },
    avatar: {
        width: 64,
        height: 64,
        borderRadius: 50
    },
    textWrapper: {
        justifyContent: 'center',
        marginHorizontal: 10
    },
    name: {
        fontSize: 18,
        fontWeight: "700",
        marginBottom: 8 
    },
    description: {
        fontSize: 16,
        color: colors.darkGrey
    },
})

export default ListItem