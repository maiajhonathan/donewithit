import React from 'react'
import { StyleSheet, Text, TouchableHighlight } from 'react-native'

import colors from '../config/colors'

const AppButton = ({ onPress, text, bgColor = colors.primary }) => {   
    return (
        <TouchableHighlight
          activeOpacity={ 0.6 }
          underlayColor={ bgColor - 1 }
          onPress={ onPress }
          style={[styles.roundButton, { backgroundColor: bgColor }]}>
            <Text style={styles.buttonText}>{ text }</Text>
        </TouchableHighlight>        
    )
}

const styles = StyleSheet.create({
    roundButton: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: 54,
        borderRadius: 50,
        marginTop: 15
    },
    buttonText: {
        fontSize: 16,
        fontWeight: "700",
        color: colors.white,
        fontStyle: "normal",
        textTransform: 'uppercase'
    }
})

export default AppButton

