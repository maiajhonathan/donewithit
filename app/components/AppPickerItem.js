import React from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'

const AppPickerItem = ({ label, onPress }) => {
    return (
        <TouchableOpacity style={styles.container} onPress={onPress}>
            <Text>{label}</Text>
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        padding: 20
    }
})

export default AppPickerItem