import React from 'react'
import { FlatList, StyleSheet, View } from 'react-native'
import Card from '../components/Card'

const listings = [
    {
        id: 1,
        title: "Red jacket for sale!",
        price: "$100",
        image: require("../assets/jacket.jpg")
    },
    {
        id: 2,
        title: "Couch in great condition",
        price: "$1000",
        image: require("../assets/jacket.jpg")
    }
]

const ListingsScreen = () => {
    const renderItem = ({ item }) => <Card title={item.title} subTitle={item.price} image={item.image}/>
    

    return (
        <View style={styles.container}>
            <FlatList
                data={listings}
                keyExtractor={listing => listing.id.toString()}
                renderItem={renderItem}
            />
        </View>
        
    )
}

const styles = StyleSheet.create({
    container: {
        padding: 20
    }
})

export default ListingsScreen