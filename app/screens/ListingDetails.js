import React from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import ListItem from '../components/ListItem';
import colors from '../config/colors';

const ListingDetails = (props) => {
    const { image, title, price } = props;


    return (
        <View style={styles.container}>
            <Image style={styles.image} source={image}/>
            <View style={styles.textWrapper}>
                <Text style={styles.title}>{ title }</Text>
                <Text style={styles.price}>{ price }</Text>
            </View>
            <View style={ styles.sellerBadge }>
                <ListItem />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    image: {
        width: "100%",
        height: 250
    },
    textWrapper: {
        padding: 20
    },
    title: {
        fontSize: 18,
        fontWeight: "700"
    },
    price: {
        fontSize: 16,
        fontWeight: "700",
        marginTop: 10,
        color: colors.secondary
    },
    sellerBadge: {
        top: 50,
        left: 20
    }
})

export default ListingDetails
