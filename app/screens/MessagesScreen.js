import React, { useState } from 'react'
import { FlatList, StyleSheet } from 'react-native'

import ListItem from '../components/ListItem'
import ListItemDeleteAction from '../components/ListItemDeleteAction'
import ItemSeparator from '../components/ItemSeparator'

const initialMessages = [
    {
        id: 1,
        title: "T1",
        description: "D1",
        image: require("../assets/mosh.jpg")
    },
    {
        id: 2,
        title: "T2",
        description: "D2",
        image: require("../assets/mosh.jpg")
    }
]

const MessagesScreen = () => {
    const [messages, setMessages] = useState(initialMessages);
    const [refreshing, setRefreshing] = useState(false);

    const handleDelete = (message) => {
        // Delete the message from messages
        setMessages(messages.filter(m => m.id !== message.id))
    }

    return (
        <FlatList 
            data={messages}
            keyExtractor={message => message.id.toString()}
            renderItem={({item}) => 
            <ListItem 
                title={item.title}
                description={item.description}
                image={item.image}
                onPress={() => console.log(item)}
                renderRightActions={ () =>
                    <ListItemDeleteAction 
                        onPress={() => handleDelete(item)}
                    />
                }
            />}
            ItemSeparatorComponent={ItemSeparator}
            refreshing={refreshing}
            onRefresh={() => setMessages(initialMessages)}
        />
    )
}

const styles = StyleSheet.create({})

export default MessagesScreen;