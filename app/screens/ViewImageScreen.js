import React from 'react';
import { Image, StyleSheet, View } from 'react-native';
import { MaterialCommunityIcons } from "@expo/vector-icons";

import colors from "../config/colors";

const ViewImageScreen = () => {
    return (
        <View style={styles.container}>
            <MaterialCommunityIcons
                name="close" 
                size={34}
                color={colors.white} 
                style={styles.closeButton} />
            <MaterialCommunityIcons 
                name="trash-can-outline"
                size={34} 
                color={colors.white} 
                style={styles.deleteButton} />
            <Image style={styles.image} resizeMode="contain" source={require("../assets/chair.jpg")} ></Image>
        </View>
    )
}

export default ViewImageScreen

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: colors.black
    },
    closeButton: {
        position: 'absolute',
        top: 50,
        left: 20,
    },
    deleteButton: {
        position: 'absolute',
        top: 50,
        right: 20,
    },
    image: {
        width: "100%",
        marginTop: 50
    }
})
