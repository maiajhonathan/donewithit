import React from 'react'
import { SafeAreaView, StyleSheet } from 'react-native'

import Constants from 'expo-constants'


const SafeAreaScreen = ({children}) => {
    return (
        <SafeAreaView style={styles.safeArea}>
            { children }
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    safeArea: {
        flex: 1,
        paddingTop: Constants.statusBarHeight
    }
})

export default SafeAreaScreen