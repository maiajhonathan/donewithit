import React from 'react';
import { Image, ImageBackground, StyleSheet, Text, View } from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import AppButton from '../components/AppButton';

import colors from '../config/colors';

const WelcomeScreen = () => {
    return (
        <ImageBackground 
          source={require("../assets/background.jpg")} 
          style={styles.background}
          blurRadius={4}
          >
            <View style={styles.logoWrapper}>
                <Image source={require("../assets/logo-red.png")} style={styles.logo}></Image>
                <Text style={styles.logoText}>Sell What You Don't Need.</Text>
            </View>
            <View style={styles.buttonWrapper}>
                <AppButton
                text="Login"
                style={styles.button}
                onPress={() => alert('Pressed Login!')} />

                <AppButton
                text="Register"
                bgColor={colors.secondary}
                styles={{marginTop: 10}}
                onPress={() => alert('Pressed Register!')} />
            </View> 
        </ImageBackground>
    )
}

export default WelcomeScreen

const styles = StyleSheet.create({
    background: {
        position: 'relative',
        flex: 1,
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    logoWrapper: {
        position: 'absolute',
        top: 70,
        alignItems: 'center',
        justifyContent: 'center'
    },
    logo: {
        width: 100,
        height: 100
    },
    logoText: {
        marginTop: 20,
        fontSize: 24,
        fontWeight: "700"
    },
    buttonWrapper: {
        paddingHorizontal: 14,
        paddingVertical: 20,
        width: '100%'
    },
    button: {
        marginVertical: 10
    }
})
