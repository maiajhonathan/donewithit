import React from 'react'
import { Image, StyleSheet, Text, View } from 'react-native'

import colors from '../config/colors'
import MenuItem from '../components/MenuItem'

const AccountScreen = (props) => {
    const user = {
        name: "Mosh Hamadani",
        email: "justanemail@gmail.com",
        image: require("../assets/mosh.jpg")
    }

    const menuItems = [
        {
            id: 1,
            label: 'My Listings',
            icon: 'format-list-bulleted',
            iconSize: 20,
            iconColor: colors.white,
            iconBgColor: colors.primary
        },
        {
            id: 2,
            label: 'My Messages',
            icon: 'email',
            iconSize: 20,
            iconColor: colors.white,
            iconBgColor: colors.secondary
        }
    ]

    return (
        <View style={styles.container}>
            <View style={ styles.profileWrapper }>
                <Image style={ styles.avatar } source={ user.image }></Image>
                <View style={ styles.textWrapper }>
                    <Text style={ styles.name }>{ user.name }</Text>
                    <Text style={ styles.email }>{ user.email }</Text>
                </View>
            </View>
            <View style={styles.menuContainer}>
                <MenuItem label="My Listings" icon="format-list-bulleted" iconSize={20} iconColor={colors.white} iconBgColor={colors.primary}/>
                <MenuItem label="My Messages" icon="email" iconSize={20} iconColor={colors.white} iconBgColor={colors.secondary}/>
            </View>
            <MenuItem label="Log Out" icon="logout" iconSize={20} iconColor={colors.black} iconBgColor={colors.yellow}/>
        </View>
    )
}

const styles = StyleSheet.create({
    profileWrapper: {
        marginVertical: 30,
        paddingHorizontal: 15,
        flexDirection: 'row',
        paddingVertical: 15,
        backgroundColor: colors.white
    },
    avatar: {
        width: 64,
        height: 64,
        borderRadius: 50
    },
    textWrapper: {
        justifyContent: 'center',
        marginHorizontal: 10
    },
    name: {
        fontSize: 18,
        fontWeight: "700",
        marginBottom: 8 
    },
    email: {
        fontSize: 16,
        color: colors.darkGrey
    },
    menuContainer: {
        marginBottom: 60
    }
})

export default AccountScreen

